package webstat

import (
	"errors"
	"net/http"
	"time"
)

var (
	client = http.Client{
		Timeout: 10 * time.Second,
	}
)

// CheckExist checks if the given url on the site exists, or returns error if the site is missing or blocked.
func CheckExist(url string) (ok bool, err error) {
	var resp *http.Response
	if resp, err = client.Get(url); err != nil {
		return
	}
	defer resp.Body.Close()

	ok = resp.StatusCode == 200
	return
}

// CheckExistWithClient checks if the given url on the site exists with the given client, or returns error if the site is missing or blocked.
func CheckExistWithClient(cli *http.Client, url string) (ok bool, err error) {
	if cli == nil {
		err = errors.New("nil http client")
		return
	}

	var resp *http.Response
	if resp, err = cli.Get(url); err != nil {
		return
	}
	defer resp.Body.Close()

	ok = resp.StatusCode == 200
	return
}
