# webstat [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/webstat)

Check if the given page is online or off.

## install

```bash
go get -d bitbucket.org/ai69/webstat
```
