package webstat

import (
	"net/http"
	"testing"
)

var urls = []string{
	"https://google.com/",
	"https://bitbucket.org/",
	"https://bitbucket.org/ai69/webstat",
	"https://github.com/cb_m123eHiu2Gbkaat6",
	"https://z4XETZpnKVcotpQN0.com/",
}

func TestCheckStatus(t *testing.T) {
	for _, url := range urls {
		if ok, err := CheckExist(url); err == nil {
			t.Logf("got check %q: %v", url, ok)
		} else {
			t.Logf("failed to check %q: %v", url, err)
		}
	}
}

func TestCheckExistWithClient(t *testing.T) {
	cli := http.DefaultClient
	for _, url := range urls {
		if ok, err := CheckExistWithClient(cli, url); err == nil {
			t.Logf("got check %q: %v", url, ok)
		} else {
			t.Logf("failed to check %q: %v", url, err)
		}
	}
}
